<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="../../img/server.png" height="256"  alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">{{ $server->hostname }}</h5>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">{{ $server->ip }}</li>
    <li class="list-group-item">{{ $server->zone }}</li>
    @if( $server->actif==1 )
     <li class="list-group-item">Actif</li>
    @else
     <li class="list-group-item">Disabled</li>
    @endif
  </ul>
</div>