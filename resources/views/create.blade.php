@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Seedbox admin create server page</h2>
  

@include('subviews.formerror')

<form method="post" class="form-horizontal" action="{{ route('store') }}">
    {{ csrf_field() }}
  <div class="form-group">
    <label class="control-label col-sm-2" for="hostname">Hostname:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="hostname" placeholder="hostname" >
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="ip">Adresse IP:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="ip" placeholder="Adresse IP" >
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="zone">Zone</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="zone" placeholder="Zone" >
    </div>
  </div>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label><input type="checkbox" name="actif" checked > Actif</label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
</form>
</div>
@endsection
