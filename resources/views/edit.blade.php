@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Seedbox admin edit servers page</h2>
  <p>Use action button to set up your server.</p>            


@include('subviews.formerror')


<form method="post" class="form-horizontal" action="{{ route('update', $server->id) }}">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}
  <div class="form-group">
    <label class="control-label col-sm-2" for="hostname">Hostname:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="hostname" placeholder="hostname" value="{{ $server->hostname }}">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="ip">Adresse IP:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="ip" placeholder="Adresse IP" value="{{ $server->ip }}">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="zone">Zone</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="zone" placeholder="Zone" value="{{ $server->zone }}">
    </div>
  </div>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label><input type="checkbox" name="actif" {{ $server->actf !== true ? 'checked' : '' }}> Actif</label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>
</div>
@endsection




