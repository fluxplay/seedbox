@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
<div class="container">
  <h2>Seedbox admin servers page</h2>
  <p>Use action button to manage your server.</p>            
  <p><a href="{{ route('create') }}"><button type="button" class="btn btn-primary">Add a new server into the parc</button></a></p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Id</th>
        <th>Hostname</th>
        <th>Ip</th>
        <th>Zone</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($servers as $server)
            <tr>
                <td>{{ $server->id }}</td>
                <td><a href=" {{route('show',$server->id)}}">{{ $server->hostname }}</a></td>
                <td>{{ $server->ip }}</td>
                <td>{{ $server->zone }}</td>
                <td>
                <a href="{{ route('edit',$server->id)}}"><button type="button" class="btn btn-success">Edit</button></a>
                <a href="{{ route('destroy',$server->id)}}"><button type="button" class="btn btn-danger">Remove</button></a>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





