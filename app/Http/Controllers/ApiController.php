<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Server;
use Validator;

/**
 * @OA\Info(
 *    title="Api seedbox",
 *    version="1.0.0",
 * )
 */

class ApiController extends Controller
{

/**
 * @OA\Get(
 * path="/servers",
 * summary="show server details",
 * description="show server details",
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="data", type="object", ref="")
 *        )
 *     ),
 * @OA\Response(
 *    response=401,
 *    description="User should be authorized to get profile information",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
    public function index(){
        $servers = Server::all();
        return response()->json($servers);
    }

    /**
 * @OA\Get(
 * path="/servers/{Id}",
 * summary="show server details",
 * description="show server details",
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="data", type="object", ref="")
 *        )
 *     ),
 * @OA\Response(
 *    response=401,
 *    description="User should be authorized to get profile information",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
    public function show($id){
        $server = Server::Find($id);
        return response()->json($server);
    }

/**
 * @OA\Post(
 * path="/servers",
 * summary="store a new server",
 * description="store a new server",
 * @OA\Parameter(
 *    name="hostname",
 *    description="the hostname of the server",
 *    in="path",
 *    required=true,
 *    example="Server001"
  * ),
 *  @OA\Parameter(
 *    description="Ip address of the server",
 *    in="path",
 *    name="Ip",
 *    required=true,
 *    example="192.168.52.2"
 * ),
 * @OA\Parameter(
 *    description="Zone where the server will be configured",
 *    in="path",
 *    name="zone",
 *    required=true,
 *    example="EXT",
 * ),
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="id", type="integer", example="1"),
 *       @OA\Property(property="hostname", type="string", example="Linux52"),
 *       @OA\Property(property="ip", type="string", example="192.168.2.52"),
 *       @OA\Property(property="zone", type="string", example="DMZ")
 *        )
 *     ),
 * @OA\Response(
 *    response=404,
 *    description="User should provide a correct server details",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="at least one parameter is not correct"),
 *    )
 * )
 * )
 */
    public function store(Request $request){

        $valid = Validator::make($request->all(), [
            'hostname'=>'required|max:50',
            'ip'=>'required|ip',
            'zone'=>'required|max:50'
            ]);

        $b_checkbox = $request->get('actif')? 1 : 0 ;

        if($valid->fails()){
            return response()->json(
                ['error' => $valid->errors()],
                401
            );
        }
        $server = new Server([
            'hostname'      => $request->input('hostname'),
            'zone'          => $request->input('zone'),
            'ip'            => $request->input('ip'),
            'actif'         => $b_checkbox,
        ]);

        $server->save();

        return response()->json($server);
    }

/**
 * @OA\Put(
 * path="/servers/{id}",
 * summary="update an existing server",
 * description="update an existing server",
 * @OA\Parameter(
 *    name="hostname",
 *    description="the hostname of the server",
 *    in="path",
 *    required=true,
 *    example="Server001"
  * ),
 *  @OA\Parameter(
 *    description="Ip address of the server",
 *    in="path",
 *    name="Ip",
 *    required=true,
 *    example="192.168.52.2"
 * ),
 * @OA\Parameter(
 *    description="Zone where the server will be configured",
 *    in="path",
 *    name="zone",
 *    required=true,
 *    example="EXT",
 * ),
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="id", type="integer", example="1"),
 *       @OA\Property(property="hostname", type="string", example="Linux52"),
 *       @OA\Property(property="ip", type="string", example="192.168.2.52"),
 *       @OA\Property(property="zone", type="string", example="DMZ")
 *        )
 *     ),
 * @OA\Response(
 *    response=401,
 *    description="User should provide a correct server Id",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="the server id is not found"),
 *    )
 * )
 * )
 */
    public function update(Request $request){
        $server = Server::Find($request->input('id'));
        if(empty($server->id)){
            return response()->json(
                ['message' => 'the server Id is not found'],
                404
            ); 
        }

        $server->hostname = $request->input('hostname');
        $server->ip = $request->input('ip');
        $server->zone = $request->input('zone');
        $server->save();

        return response()->json($server);
    }

    /**
 * @OA\Delete(
 * path="/servers/{Id}",
 * summary="Delete an existing server",
 * description="Delete an existing server",
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="id", type="integer", example="1"),
 *       @OA\Property(property="hostname", type="string", example="Linux52"),
 *       @OA\Property(property="ip", type="string", example="192.168.2.52"),
 *       @OA\Property(property="zone", type="string", example="DMZ")
 *        )
 *     ),
 * @OA\Response(
 *    response=401,
 *    description="User should provide a correct server Id",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="the server id is not found"),
 *    )
 * )
 * )
 */
    public function delete(Request $request){
        $server = Server::Find($request->input('id'));
        if(empty($server->id)){
            return response()->json(
                ['message' => 'the server Id is not found'],
                404
            ); 
        }

        $server->delete();
        $servers = Server::all();
        return response()->json($servers);
    }

}
