<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Server;

class ServerController extends Controller
{

    function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servers = Server::where('actif',1)->get();
        return view('index', compact('servers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'hostname'=>'required|max:50',
            'ip'=>'required|ip',
            'zone'=>'required|max:50'
        ]);
        $b_checkbox = $request->get('actif')? 1 : 0 ;
        $server = new Server([
            'hostname'      => $request->get('hostname'),
            'zone'          => $request->get('zone'),
            'ip'            => $request->get('ip'),
            'actif'         => $b_checkbox,
        ]);

        $server->save();

        $servers = Server::where('actif',1)->get();
        return view('index', compact('servers'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $server = Server::find($id);
        return view('show', compact('server'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $server = Server::find($id);
        return view('edit', compact('server'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'hostname'=>'required|max:50',
            'ip'=>'required|ip',
            'zone'=>'required|max:50'
        ]);
        $b_checkbox = $request->get('actif')? 1 : 0 ;

        $server = Server::find($id);
        if(!empty($server->id)){
            $server->hostname = $request->get('hostname');
            $server->ip       = $request->get('ip');
            $server->zone     = $request->get('zone');
            $server->actif    = $b_checkbox;

            $server->save();
        }

        $servers = Server::where('actif',1)->get();
        return view('index', compact('servers'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $server = Server::find($id);
        if(!empty($server->id)){
            $server->delete();
        }

        $servers = Server::where('actif',1)->get();
        return view('index', compact('servers'));
        }
}
