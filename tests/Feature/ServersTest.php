<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Server;

class ServerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guests_cant_see_the_servers_list()
    {
        $response = $this->get('/servers')->assertRedirect('/login');
    }

    /** @test */
    public function only_admin_can_see_the_servers_list()
    {
        $this->actingAs(factory(User::class)->create(['email'=>'admin@test.com',]));

        $response = $this->get('/servers')->assertOk();
    }

    /** @test */
    public function admin_can_create_a_new_server()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->post('/servers', $this->serverData());

        $this->assertCount(1, Server::all());
    }

       /** @test */
        public function admin_can_modify_an_existing_server()
        {
            $this->actingAs(factory(User::class)->create());

            $response = $this->post('/servers', $this->serverData());
            
            $this->assertCount(1, Server::all());

            $server_updated = array_merge($this->serverData(),[
                'hostname'=>'firewall02', // hostname updated
            ]);

            $response = $this->patch('/servers/update/1',$server_updated);

            $this->assertEquals(Server::find(1)->first()->hostname, 'firewall02');
        }


        public function serverData(){
            return [
                'hostname' => 'firewall01',
                'ip' => '192.168.4.52',
                'zone' => 'EXT',
                'actif' => 1,
            ];
        }
}
