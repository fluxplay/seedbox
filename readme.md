## Introduction 

This software has been created using couple of tools 
- Laravel
- Mysql
- PhpUnit 
- Swagger
- Chrome
- PostMan

The demo website is available here: http://v2.fluxplay.fr:8083/public/servers

Find here more explanation about the projet: [explications.md](explications.md)


## Getting started

Clone the project and start Laravel server

```bash
git clone git@gitlab.com:fluxplay/seedbox.git
php artisan server
```

## Create database

Using Mysql, create a database called 'seedbox'

```sql
create database 'seedbox' ;
```

Generate the Laravel migration to create tables 

```bash
php artisan migration
```

Insert some default servers

```sql
INSERT INTO `seedbox`.`servers` (`id`, `hostname`, `ip`, `zone`, `actif`, `created_at`, `updated_at`) VALUES ('1', 'linux1', '192.168.15.52', 'DMZ', '1', '2020-09-15', '2020-09-15');
INSERT INTO `seedbox`.`servers` (`id`, `hostname`, `ip`, `zone`, `actif`, `created_at`, `updated_at`) VALUES ('2', 'linux2', '192.168.15.89', 'DMZ', '0', '2020-09-15', '2020-09-15');
INSERT INTO `seedbox`.`servers` (`id`, `hostname`, `ip`, `zone`, `actif`, `created_at`, `updated_at`) VALUES ('3', 'Windows1', '192.168.15.25', 'DMZ', '1', '2020-09-15', '2020-09-15');
INSERT INTO `seedbox`.`servers` (`id`, `hostname`, `ip`, `zone`, `actif`, `created_at`, `updated_at`) VALUES ('4', 'hadoop1', '192.168.15.10', 'EXT', '1', '2020-09-15', '2020-09-15');

```


## Create database for Unit tests

Using Mysql, create a database called 'seedbox'

```sql
create database 'seedboxtst' ;
```




## Use the Swagger for Api documentation

http://v2.fluxplay.fr:8083/public/api/docs


## Executes Unit Tests

Let's go to the PhpUnit folder and execute the testSuite.

```bash
vendor/bin/phpunit --filter admin_can_modify_an_existing_server
vendor/bin/phpunit --filter guests_cant_see_the_servers_list
vendor/bin/phpunit --filter only_admin_can_see_the_servers_list
vendor/bin/phpunit --filter admin_can_create_a_new_server
vendor/bin/phpunit --filter admin_can_modify_an_existing_server
```

Or

```bash
vendor/bin/phpunit
```

You should get somethink like this

```bash
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

....R                                                               6 / 6 (100%)

Time: 6.94 seconds, Memory: 26.00 MB

There was 1 risky test:

1) Tests\Feature\ServerTest::serverData
This test did not perform any assertions

/var/www/html/tests/Feature/ServersTest.php:57

OK, but incomplete, skipped, or risky tests!
Tests: 6, Assertions: 6, Risky: 1.
```


