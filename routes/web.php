<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*seedbox*/
Route::get('/home', 'ServerController@index')->name('index');
Route::get('/servers', 'ServerController@index')->name('index');
Route::get('/servers/show/{id}', 'ServerController@show')->name('show');
Route::get('/servers/edit/{id}', 'ServerController@edit')->name('edit');
Route::get('/servers/create', 'ServerController@create')->name('create');
Route::get('/servers/destroy/{id}', 'ServerController@destroy')->name('destroy');
Route::post('/servers', 'ServerController@store')->name('store');
Route::patch('/servers/update/{id}', 'ServerController@update')->name('update');
