
# Introduction

 L'architecture utilisé ici est le framework php Laravel pour un gestion rapide de la couche d'authentification et de l'ORM pour une meilleure interface entre la couche applicative et les données. La kernel du framework nous permet aussi la gestion du routage des requêtes HTTP. 

De cette façon nous pouvons utiliser n'importe quel client qui pourra envoyer des requêtes HTTP, dans notre exemple nous allons pourvoir créer, lister, et modifier la liste des servers en utilisant un navigateur classique mais aussi un outil comme Postman grâce a l'Api crée avec Laravel.

Pour la persistance des données j'ai utilisées une base de données relationnelle MySql avec une database 'seedbox' et une autre database 'seedboxtst' pour les tests Unitaires. 

Les tests unitaire ont étaient réalisés avec PhpUnit et la base de données 'seedboxtst'


# Environement de developpement 

- Docker
- Mysql 
- Visual Code
- Linux (Ubuntu)


# Comment la zone d'administration est elle sécurisé.

Grace un login et mot de passe qui sécurise l’accès au site et donc l’accès au fonctions de listage, de création et de modification des serveurs. 

L’accès a l'Api est aussi effectué par login est mot de passe. 

J'ai voulu avoir la même stratégie d'authentification pour l'Api et l’accès au site Laravel. Une amélioration aurait pu être d'utiliser avec OAuth2 et le chiffrage du token avec du RSA-256.



# Authentification avec PostMan

Onglet Authentification->BasicAuth

login : admin@gmail.com

mot de passe : admin1234




# Authentification sur le site

login : admin@gmail.com

mot de passe : admin1234

http://v2.fluxplay.fr:8083/public/servers





# Services applicatif

Le but de ce site est d'administrer les servers d'une entreprise qui grossie.  

-Créer un server
-Modifier un serveur actifs
-Afficher des serveurs actifs
-Supprimer un serveur actifs



## Amélioration

Le Model 'Server' aurait pu être bcp plus élaboré, en ajoutant son rôle, sa gatway réseau, le port sur lequel le service web est actif et bien d'autres option.  



# Utilisation de Laravel

Apres être identifié utiliser les routes suivantes pour administrer les serveurs.

Lister les serveurs actifs : http://v2.fluxplay.fr:8083/public/servers

Créer un nouveau server : http://v2.fluxplay.fr:8083/public/servers/create

Modifier un server dont l'Id est X: http://v2.fluxplay.fr:8083/public/servers/edit/X

Supprimer un serveur actif dont l'Id est X: http://v2.fluxplay.fr:8083/public/servers/destroy/X




# Utilisation avec l'Api 

Utiliser la méthode d’authentification BasicAuth

Pour faciliter la lecture des services, j'ai ajouté un Swagger pour documenter l'Api 
http://v2.fluxplay.fr:8083/public/api/doc

## Amelioration

Les requetes via le swagger ne marche pas car je n'ai pas eu le temps de configurer l'authentification. Mais on utilisant PostMan l'Api est 100% fonctionelle. 



# Tests unitaires

Apres avoir executé la commande phpunit j'ai redirigé les resultats dans un fichier texte qui est consultable. 

```bash
vendor/bin/phpunit > phpunit_results.txt
```

http://v2.fluxplay.fr:8083/phpunit_results.txt



## Amelioration

Les tests sont tres primaires et j'aurai pu creer des tests de verification sur les chaines 'hostname' et 'zone' ainsi que sur le format de l'adresse ip qui est validé en interne par Laravel. 



